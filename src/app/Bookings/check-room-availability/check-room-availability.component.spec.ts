import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckRoomAvailabilityComponent } from './check-room-availability.component';

describe('CheckRoomAvailabilityComponent', () => {
  let component: CheckRoomAvailabilityComponent;
  let fixture: ComponentFixture<CheckRoomAvailabilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckRoomAvailabilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckRoomAvailabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
