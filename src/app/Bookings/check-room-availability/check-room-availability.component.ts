import {Component, Input, OnInit} from '@angular/core';
import {HotelManagementService} from '../../hotel-management.service';
import {ActivatedRoute} from '@angular/router';
import {Enquiry} from '../../model/enquiry';

@Component({
  selector: 'app-check-room-availability',
  templateUrl: './check-room-availability.component.html',
  styleUrls: ['./check-room-availability.component.scss']
})
export class CheckRoomAvailabilityComponent implements OnInit {

  @Input()
  enquiry: any = {};
  code: any;
  allAvailableRooms: any;
  constructor(private service: HotelManagementService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(
      data => this.code = data.code
    );
    console.log(this.code);
    this.getAllAvailableRooms();
  }
  getAllAvailableRooms() {
    this.service.searchRoomAvailability(this.code).subscribe(
      data => this.allAvailableRooms = data
    );
  }
  chekingEnquiryDetails() {
    return this.enquiry;
  }

  check() {
    console.log(this.enquiry);
  }
}

