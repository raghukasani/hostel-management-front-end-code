import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HotelManagementService} from '../../hotel-management.service';
import {RoomType} from '../../model/room-type';
import {Router} from '@angular/router';
import {Enquiry} from '../../model/enquiry';

@Component({
  selector: 'app-enquiry',
  templateUrl: './enquiry.component.html',
  styleUrls: ['./enquiry.component.scss']
})
export class EnquiryComponent implements OnInit {
   createEnquiryForm: FormGroup;
   allRoomTypes: RoomType[] = [];
   enquiryDetails: Enquiry;
   roomTypeDetailsOnSearch: any;
   codeForCurrentRoom: any;
   enquiryFormBinding: Enquiry;

  constructor(private formBuilder: FormBuilder, private service: HotelManagementService, private router: Router) { }

  ngOnInit() {
    this.formForCreateEnquiy();
    this.getAllRoomTypes();
  }

  formForCreateEnquiy() {
    this.createEnquiryForm = this.formBuilder.group({
      id: [null, null],
      roomType: [null, Validators.required],
      fromDate: [null, Validators.required],
      toDate: [null, Validators.required],
      numOfPersons: [null, Validators.required]
    });
  }
  getAllRoomTypes() {
   this.service.getAllRoomTypes().subscribe(
     data => this.allRoomTypes = data
   );
  }

  roomTypeEnquiry() {
    this.enquiryDetails = this.createEnquiryForm.value;
    this.service.searchRoomType(this.createEnquiryForm.value.roomType).subscribe(
     data => this.roomTypeDetailsOnSearch = data
   );
  }

  reset() {
    this.createEnquiryForm.reset();
  }
  navigateToCheckingAvilabilitypage(code: string) {

    this.enquiryFormBinding = this.enquiryDetails;
    console.log( this.enquiryFormBinding)
    this.codeForCurrentRoom = code;
    console.log(this.codeForCurrentRoom);
    this.router.navigateByUrl('check-room-availability/' + code );
  }
}
