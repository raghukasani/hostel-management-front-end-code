import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {HotelManagementService} from '../../hotel-management.service';
import {MessageService} from 'primeng/api';
import {ReservedRoom} from '../../model/reserved-room';

@Component({
  selector: 'app-reserve-room',
  templateUrl: './reserve-room.component.html',
  styleUrls: ['./reserve-room.component.scss']
})
export class ReserveRoomComponent implements OnInit {
  private createReserveForm: FormGroup;
  selectedfromDate: string;
  selectedToDate: string;
  reserveRoom: ReservedRoom;
  constructor(private formBuilder: FormBuilder, private datePipe: DatePipe, private service: HotelManagementService, private messageService: MessageService) { }

  ngOnInit() {
    this.formForCreateReservation();
  }

  formForCreateReservation() {
    this.createReserveForm = this.formBuilder.group({
      id: [null, null],
      roomNumber: [null, Validators.required],
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      emailId: [null, Validators.required],
      phoneNumber: [null, Validators.required],
      fromDate: [null, Validators.required],
      toDate: [null, Validators.required]
    });
  }

  getData() {
    console.log(this.createReserveForm.value);
    this.reserveRoom = this.createReserveForm.value;
    console.log(this.reserveRoom);
    // tslint:disable-next-line:max-line-length
    if (this.reserveRoom.roomNumber == null || this.reserveRoom.firstName == null && this.reserveRoom.lastName == null && this.reserveRoom.phoneNumber == null &&
    this.reserveRoom.emailId == null && this.reserveRoom.fromDate == null && this.reserveRoom.toDate ==  null) {
      return this.messageService.add({severity: 'warn', summary: 'warn Message', detail: 'Please Fill All the Fields..'});
    } else {
      this.service.reserveRoom(this.createReserveForm.value).subscribe(
        data => {
          return this.messageService.add({severity: 'success', summary: 'success Message', detail: 'Room Reserved Successfully..'});
        }
      );
      this.createReserveForm.reset();
    }
    }

}
