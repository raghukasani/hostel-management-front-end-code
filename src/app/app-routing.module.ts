import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CreateRoomTypeComponent} from './create-room-type/create-room-type.component';
import {RoomTypeListComponent} from './room-type-list/room-type-list.component';
import {CreateRoomComponent} from './create-room/create-room.component';
import {RoomsListComponent} from './rooms-list/rooms-list.component';
import {BedsComponent} from './beds/beds.component';
import {BedTypeListComponent} from './bed-type-list/bed-type-list.component';
import {CreateBedTypeComponent} from './create-bed-type/create-bed-type.component';
import {EnquiryComponent} from './Bookings/enquiry/enquiry.component';
import {CheckRoomAvailabilityComponent} from './Bookings/check-room-availability/check-room-availability.component';
import {ReserveRoomComponent} from './Reserve/reserve-room/reserve-room.component';


const routes: Routes = [
  {
    path: 'create-room-type',
    component: CreateRoomTypeComponent
  },
  {
    path: 'Update-RoomType/:id',
    component: CreateRoomTypeComponent
  },
  {
    path: 'delete-RoomType/:id',
    component: RoomTypeListComponent
  },
  {
    path: 'create-room',
    component: CreateRoomComponent
  },
  {
    path: 'room-type-list',
    component: RoomTypeListComponent
  },
  {
    path: 'rooms-list',
    component: RoomsListComponent
  },
  {
    path: 'Update-Room/:id',
    component: CreateRoomComponent
  },
  {
    path: 'delete-Room/:id',
    component: RoomsListComponent
  },
  {
    path: 'beds',
    component: BedsComponent
  },
  {
    path: 'delete-Bed/:id',
    component: BedsComponent
  },
  {
    path: 'bed-type-list',
    component: BedTypeListComponent
  },
  {
    path: 'create-bed-type',
    component: CreateBedTypeComponent
  },
  {
    path: 'Update-BedType/:id',
    component: CreateBedTypeComponent
  },
  {
    path: 'delete-BedType/:id',
    component: BedTypeListComponent
  },
  {
    path: 'enquiry',
    component: EnquiryComponent
  },
  {
    path: 'check-room-availability/:code',
    component: CheckRoomAvailabilityComponent
  },
  {
    path: 'reserve',
    component: ReserveRoomComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
