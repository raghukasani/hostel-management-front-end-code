import { Component } from '@angular/core';
 ;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'hotel-management-system';
  images = [
    { img: '../assets/h1.jpg' },
    { img: '../assets/h2.jpg' },
    { img: '../assets/h3.jpg' }
  ];

  slideConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 0,
    speed: 3500
  };

}
