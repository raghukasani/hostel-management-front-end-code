import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateRoomTypeComponent } from './create-room-type/create-room-type.component';
import {FormsModule, ReactiveFormsModule } from '@angular/forms';

import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {ToastModule} from 'primeng/toast';
import {MessageService} from 'primeng/api';
import { RoomTypeListComponent } from './room-type-list/room-type-list.component';
import {TableModule} from 'primeng/components/table/table.js';
import {TabViewModule} from 'primeng/tabview';
import { CreateRoomComponent } from './create-room/create-room.component';
import { RoomsListComponent } from './rooms-list/rooms-list.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {library} from '@fortawesome/fontawesome-svg-core';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {TooltipModule} from 'primeng/tooltip';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {PanelModule} from 'primeng/panel';
import { BedsComponent } from './beds/beds.component';
import { BedTypeListComponent } from './bed-type-list/bed-type-list.component';
import { CreateBedTypeComponent } from './create-bed-type/create-bed-type.component';
import {HttpErrorInterceptor} from './http.error.interceptor';
import {CarouselModule} from 'primeng/carousel';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { EnquiryComponent } from './Bookings/enquiry/enquiry.component';

import {CalendarModule} from 'primeng/calendar';
import { CheckRoomAvailabilityComponent } from './Bookings/check-room-availability/check-room-availability.component';
import { ReserveRoomComponent } from './Reserve/reserve-room/reserve-room.component';
import { DatePipe } from '@angular/common'



@NgModule({
  declarations: [
    AppComponent,
    CreateRoomTypeComponent,
    RoomTypeListComponent,
    CreateRoomComponent,
    RoomsListComponent,
    BedsComponent,
    BedTypeListComponent,
    CreateBedTypeComponent,
    EnquiryComponent,
    CheckRoomAvailabilityComponent,
    ReserveRoomComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    InputTextModule,
    ToastModule,
    TableModule,
    TabViewModule,
    HttpClientModule,
    FontAwesomeModule,
    TooltipModule,
    ConfirmDialogModule,
    BrowserAnimationsModule,
    PanelModule,
    CarouselModule,
    SlickCarouselModule,
    CalendarModule

  ],
  providers: [ MessageService, ConfirmationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    library.add(fas);
  }
}
