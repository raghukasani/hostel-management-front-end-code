import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HotelManagementService} from '../hotel-management.service';
import {BedType} from '../model/bed-type';
import {ConfirmationService, MessageService} from 'primeng/api';

@Component({
  selector: 'app-bed-type-list',
  templateUrl: './bed-type-list.component.html',
  styleUrls: ['./bed-type-list.component.scss']
})
export class BedTypeListComponent implements OnInit {

  allBedTypes: BedType[] = [];
  constructor(private router: Router, private service: HotelManagementService, private confirmationService: ConfirmationService, private messageService: MessageService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.getAllBedTypes();
    this.activatedRoute.params.subscribe(
      id => {
        const id1 = id.id;
        if (id1) {
          this.confirmDelete(id1);
        }
      }
    );
  }

  createBedType() {
    this.router.navigateByUrl('create-bed-type');
  }
  getAllBedTypes() {
    this.service.getAllBedTypes().subscribe(
      data => this.allBedTypes = data
    );
  }
  confirmDelete(id: string) {
    console.log('deleeeeeeeeeeee');
    this.confirmationService.confirm({
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.service.deleteBedType(id).subscribe(
          data => {
            return this.messageService.add({severity: 'success', summary: 'Success Message', detail: 'Bed Type  Deleted Successfully..'});

          }
        );
      },
      reject: () => {
        return this.messageService.add({severity: 'error', summary: 'Error Message', detail: 'Bed Type Deletion failed..'});
      }
    });
  }
}
