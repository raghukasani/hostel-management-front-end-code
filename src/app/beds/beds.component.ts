import { Component, OnInit } from '@angular/core';
import {HotelManagementService} from '../hotel-management.service';
import {Beds} from '../model/beds';
import {ActivatedRoute} from '@angular/router';
import {ConfirmationService, MessageService} from 'primeng/api';

@Component({
  selector: 'app-beds',
  templateUrl: './beds.component.html',
  styleUrls: ['./beds.component.scss']
})
export class BedsComponent implements OnInit {

  allBeds: Beds[] = [];

  constructor(private service: HotelManagementService, private activatedRoute: ActivatedRoute, private confirmationService: ConfirmationService, private messageService: MessageService) {
  }

  ngOnInit() {
    this.retrieveAllBeds();
    this.activatedRoute.params.subscribe(
      id => {
        const id1 = id.id;
        console.log(id1);
        if (id1) {
          this.confirmDelete(id1);
        }
      }
    );
  }

  retrieveAllBeds() {
    this.service.getAllBeds().subscribe(
      data => this.allBeds = data
    );
  }

  private confirmDelete(id1: string) {
    console.log('deleeeeeeeeeeee');
    this.confirmationService.confirm({
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.service.deleteBed(id1).subscribe(
          data => {
            return this.messageService.add({severity: 'success', summary: 'Success Message', detail: 'Room  Deleted Successfully..'});
          }
        );
      },
      reject: () => {
        return this.messageService.add({severity: 'error', summary: 'Error Message', detail: 'Room  Deletion Failed..'});
      }
    });
  }
}
