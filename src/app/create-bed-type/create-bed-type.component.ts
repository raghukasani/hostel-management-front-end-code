import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BedType} from '../model/bed-type';
import {HotelManagementService} from '../hotel-management.service';
import {MessageService} from 'primeng/api';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-create-bed-type',
  templateUrl: './create-bed-type.component.html',
  styleUrls: ['./create-bed-type.component.scss']
})
export class CreateBedTypeComponent implements OnInit {
   createBedTypeForm: FormGroup;

  constructor(private  formBuilder: FormBuilder, private  service: HotelManagementService, private messageService: MessageService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.formForCreateBedType();
    this.activatedRoute.params.subscribe(
      id => {
        const id1 = id.id;
        console.log(id1);
        if (id1 == null) {} else {
          this.service.getBedTypeById(id1).subscribe(
            data => {
              const bedType = data;
              console.log(bedType);
              this.createBedTypeForm.patchValue(bedType);

            }
          );
        }
      }
    );
  }
  formForCreateBedType() {
    this.createBedTypeForm = this.formBuilder.group({
      id: [null, null],
      code: [null, Validators.required],
      description: [null, Validators.required],
      numOfPersons: [null, Validators.required],
      location: [null, Validators.required],
    });
  }

  createBedType() {
   const bedType: BedType = this.createBedTypeForm.value;
   console.log(bedType.id);
   if (bedType.id) {
      this.service.updateBedType(bedType).subscribe(
        data => {
          console.log(data);
          alert('Bed Type updated successfully');
        }
      );
    } else {
     if (bedType.code == null || bedType.description == null || bedType.numOfPersons == null || bedType.location == null) {
       return this.messageService.add({severity: 'warn', summary: 'warn Message', detail: 'Please Fill All The Fields..'});
     } else {
       this.service.addBedType(bedType).subscribe(
         data => {
           return this.messageService.add({severity: 'success', summary: 'Success Message', detail: 'Bed Type Created Successfully..'});
         }
       );
     }
     this.createBedTypeForm.reset();
     }
  }
  reset() {
    this.createBedTypeForm.reset();
  }

}
