import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HotelManagementService} from '../hotel-management.service';
import {RoomType} from '../model/room-type';
import {ActivatedRoute} from '@angular/router';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-create-room-type',
  templateUrl: './create-room-type.component.html',
  styleUrls: ['./create-room-type.component.scss']
})
export class CreateRoomTypeComponent implements OnInit {


  createRoomTypeForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private service: HotelManagementService, private  activatedRoute: ActivatedRoute, private  messageService: MessageService) { }

  ngOnInit() {
    this.formForCreateRoomType();
    this.activatedRoute.params.subscribe(
    id => {
      const id1 = id.id;
      console.log(id1);
      if (id1 == null) {} else {
          this.service.getRoomTypeById(id1).subscribe(
            data => {
              const roomType = data;
              console.log(roomType);
              this.createRoomTypeForm.patchValue(roomType);

            }
            );
      }
    }
    );
  }
   formForCreateRoomType() {
    this.createRoomTypeForm = this.formBuilder.group({
      id: [null, null],
       code: [null, Validators.required],
      description: [null, Validators.required],
      bathroom: [null, Validators.required],
      numOfGuests: [null, Validators.required],
      defaultPrice: [null, Validators.required]
    });
   }
  createRoomType() {
    console.log(this.createRoomTypeForm.value);
    const roomType: RoomType = this.createRoomTypeForm.value;
    console.log(roomType.id);
    if (roomType.id) {
      this.service.updateRoomType(roomType).subscribe(
        data => {
          console.log(data);
          alert('Room Type updated successfully');
        }
      );
    } else {
      if (roomType.code == null || roomType.description == null || roomType.bathroom == null || roomType.numOfGuests == null || roomType.defaultPrice == null) {
        return this.messageService.add({severity: 'warn', summary: ' warn Message', detail: 'please Fill All The fields and click save..'});
      } else {
        this.service.addRoomType(roomType).subscribe(
          data => {
            return this.messageService.add({severity: 'success', summary: 'Success Message', detail: 'Room Type Created Successfully..'});
          }
        );
      }
    }
    this.createRoomTypeForm.reset();
  }
  reset() {
    this.createRoomTypeForm.reset();
  }

}
