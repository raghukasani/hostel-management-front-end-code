import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HotelManagementService} from '../hotel-management.service';
import {RoomType} from '../model/room-type';
import {MessageService} from 'primeng/api';
import {ActivatedRoute} from '@angular/router';
import {Room} from '../model/room';
import {BedType} from '../model/bed-type';
import {Beds} from '../model/beds';


@Component({
  selector: 'app-create-room',
  templateUrl: './create-room.component.html',
  styleUrls: ['./create-room.component.scss']
})
export class CreateRoomComponent implements OnInit {


  listOfBedTypes: BedType[] = [];
  createRoomForm: FormGroup;
  form: FormGroup;
  allRoomTypes: RoomType[] = [];
  room: Room;
   i: number;


  constructor(private formBuilder: FormBuilder, private  service: HotelManagementService, private messageService: MessageService, private activatedRoute: ActivatedRoute) {

  }

  ngOnInit() {
    this.formForCreateRoom();
    this.getAllRoomTypes();
    this.getallBedTypes();
    this.activatedRoute.params.subscribe(
      id => {
        const id1 = id.id;
        console.log(id1);
        if (id1 == null) {
        } else {
          this.service.getRoomById(id1).subscribe(
            data => {
              const room = data;
              console.log(room);
              this.populateDataIntoRoomAndBedForm(room);

            }
          );
        }
      }
    );
    this.form = this.formBuilder.group({
      beds: this.formBuilder.array([])
    });
  }

  formForCreateRoom() {
    this.createRoomForm = this.formBuilder.group({
      id: [null, null],
      roomNumber: [null, Validators.required],
      description: [null, Validators.required],
      roomType: [null, Validators.required],
      area: [null, Validators.required],
      bedsInRoom: [1, Validators.required],
      bedsList: [null, null]
    });
  }

  createRoom() {
    this.i = this.form.controls.beds.value.length;
    console.log(this.i);
    console.log(this.createRoomForm.value);
    this.room = this.createRoomForm.value;
    this.room.bedsInRoom = this.i;
    this.room.bedsList = this.form.controls.beds.value;
    if (this.room.id) {
      this.service.updateRoom(this.room).subscribe(
        data => {
          console.log(data);
          return this.messageService.add({severity: 'success', summary: 'Success Message', detail: 'Room updated  Successfully..'});
        }
      );
    } else {
      if (this.room.roomNumber == null || this.room.description == null || this.room.roomType == null ||
        this.room.area == null || this.room.bedsInRoom == null || this.room.bedsList.length === 0) {
        return this.messageService.add({severity: 'warn', summary: 'warn Message', detail: 'Please fill all the fields and click Save..'});
      } else {
        this.service.addRoom(this.room).subscribe(
          data => {
            return this.messageService.add({severity: 'success', summary: 'Success Message', detail: 'Room  Created Successfully..'});
          }
        );
      }
      }
  }

  reset() {
    this.createRoomForm.reset();
    this.form.controls.beds.reset();
  }

  getAllRoomTypes() {
    this.service.getAllRoomTypes().subscribe(
      data => this.allRoomTypes = data
    );
  }


  createBedForm() {
    return this.formBuilder.group({
      id: [null, null],
      code: [null, Validators.required],
      descriptions: ['', Validators.required],
      room: [this.createRoomForm.value.roomNumber, Validators.required],
      bedType: ['', Validators.required]

    });
  }
  updateBed(bed: Beds) {
    console.log(bed)
    return this.formBuilder.group({
      id: [bed.id, null],
      code: [bed.code, Validators.required],
      descriptions: [bed.descriptions, Validators.required],
      room: [bed.room, Validators.required],
      bedType: [bed.bedType, Validators.required]

    });
  }

  addNext() {

    (this.form.controls.beds as FormArray).push(this.createBedForm());
  }

  submit() {
    console.log(this.form.value);

  }

  getallBedTypes() {
    this.service.getAllBedTypes().subscribe(
      data => this.listOfBedTypes = data
    );
    console.log(this.listOfBedTypes);
  }

 populateDataIntoRoomAndBedForm(room: Room) {

    this.createRoomForm.patchValue({
      id: room.id,
      roomNumber: room.roomNumber,
      description: room.description,
      roomType: room.roomType,
      area: room.area,
      bedsInRoom: room.roomNumber,
      bedsList: this.populateBedFormOnUpdating(room.bedsList, this)
    });

 }
 populateBedFormOnUpdating(bedsList: Beds[], that) {

  const formArrayOfBeds = (this.form.controls.beds as FormArray);
  for (let bed of bedsList) {
    formArrayOfBeds.push(this.updateBed(bed));
  }
 }

}
