import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {RoomType} from './model/room-type';
import {environment} from '../environments/environment';
import {Room} from './model/room';
import {Beds} from './model/beds';
import {BedType} from './model/bed-type';
import {ReservedRoom} from './model/reserved-room';

@Injectable({
  providedIn: 'root'
})
export class HotelManagementService {

  constructor(private httpClient: HttpClient) {
  }

// ******************************Room Type Service Methods************************************************************
  getAllRoomTypes(): Observable<RoomType[]> {
    console.log('hiiiii');
    return this.httpClient.get<RoomType[]>(`${environment.url}/roomType`);
  }

  addRoomType(roomType: RoomType): Observable<RoomType> {
    return this.httpClient.post<RoomType>(`${environment.url}/roomType`, roomType);
  }

  getRoomTypeById(id: string): Observable<RoomType> {
    return this.httpClient.get<RoomType>(`${environment.url}/roomType/` + id);
  }

  updateRoomType(roomType: RoomType): Observable<RoomType> {
    return this.httpClient.put<RoomType>(`${environment.url}/roomType`, roomType);
  }

  deleteRoomType(id: string): Observable<any> {
    return this.httpClient.delete<any>(`${environment.url}/roomType/` + id);
  }

  // ********************************Rooms Service Methods*******************************************************************
  addRoom(room: Room): Observable<Room> {
    console.log(room);
    return this.httpClient.post<Room>(`${environment.url}/room`, room);
  }

  getAllRooms(): Observable<Room[]> {
    return this.httpClient.get<Room[]>(`${environment.url}/room`);
  }

  getRoomById(id1: string): Observable<Room> {
    return this.httpClient.get<Room>(`${environment.url}/room/` + id1);
  }

  updateRoom(room: Room): Observable<Room> {
    return this.httpClient.put<Room>(`${environment.url}/room`, room);
  }

  deleteRoom(id: string): Observable<any> {
    return this.httpClient.delete<any>(`${environment.url}/room/` + id);
  }

  // ********************** Beds Methods ***************
  addBed(bed: Beds): Observable<Beds> {
    console.log(bed);
    return this.httpClient.post<Beds>(`${environment.url}/bed`, bed);
  }

  getAllBeds(): Observable<Beds[]> {
    console.log('eeeeeeeeeeeeeee');
    return this.httpClient.get<Beds[]>(`${environment.url}/bed`);
  }

  getBedById(id1: string): Observable<Beds> {
    return this.httpClient.get<Beds>(`${environment.url}/bed/` + id1);
  }

  updateBed(bed: Beds): Observable<Beds> {
    return this.httpClient.put<Beds>(`${environment.url}/bed`, bed);
  }

  deleteBed(id: string): Observable<any> {
    return this.httpClient.delete<any>(`${environment.url}/bed/` + id);
  }

  // ******************* Bed Type service Methods**************************
  addBedType(bedType: BedType): Observable<BedType> {
    console.log(bedType);
    return this.httpClient.post<BedType>(`${environment.url}/bedType`, bedType);
  }

  getAllBedTypes(): Observable<BedType[]> {
    console.log('eeeeeeeeeeeeeee');
    return this.httpClient.get<BedType[]>(`${environment.url}/bedType`);
  }

  getBedTypeById(id1: string): Observable<BedType> {
    return this.httpClient.get<BedType>(`${environment.url}/bedType/` + id1);
  }

  updateBedType(bedType: BedType): Observable<BedType> {
    return this.httpClient.put<BedType>(`${environment.url}/bedType`, bedType);
  }

  deleteBedType(id: string): Observable<any> {
    return this.httpClient.delete<any>(`${environment.url}/bedType/` + id);
  }

  // ********************************************
  searchRoomType(code: string): Observable<RoomType[]> {
    return this.httpClient.get<RoomType[]>(`${environment.url}/roomTypes/by/code/` + code);
  }

  searchRoomAvailability(code: string): Observable<Room[]> {
    return this.httpClient.get<Room[]>(`${environment.url}/rooms/by/availability/false/` + code);
  }

  reserveRoom(reserveRoom: ReservedRoom): Observable<ReservedRoom> {
    console.log(reserveRoom);
    return this.httpClient.post<ReservedRoom>(`${environment.url}/reserveRoom`, reserveRoom);
  }
}
