import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';



export class HttpErrorInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(catchError(this.handleError));
  }
  // tslint:disable-next-line:no-shadowed-variable
  handleError(error) {
    if (error.status === 400) {
      alert(error.error.message)
      return throwError(error.error.message);
    } else if (error.status === 504) {
      alert(error.message);
    } else {
      alert(error.message);
    }
  }


}

