export class BedType {
  id: number;
  code: string;
  description: string;
  numOfPersons: number;
  location: string;
}
