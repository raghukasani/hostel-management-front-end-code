export class Beds {
  id: number;
  code: number;
  descriptions: string;
  room: string;
  bedType: string;
}
