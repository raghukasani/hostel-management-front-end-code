export class Enquiry {
  id: number;
  roomType: string;
  fromDate: string;
  toDate: string;
  numOfPersons: number;
}
