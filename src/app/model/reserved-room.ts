export class ReservedRoom {
  id: number;
  roomNumber: string
  firstName: string;
  lastName: string;
  emailId: string;
  phoneNumber: string;
  fromDate: string;
  toDate: string;

}
