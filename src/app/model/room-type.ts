export class RoomType {
  id: number;
  code: string;
  description: string;
  bathroom: string;
  numOfGuests: number;
  defaultPrice: number;
}
