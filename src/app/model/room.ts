import {Beds} from './beds';

export class Room {
  id: number;
  roomNumber: string;
  description: string;
  roomType: string;
  area: number;
  bedsInRoom: number;
  availability: boolean;
  bedsList: Beds[];
}
