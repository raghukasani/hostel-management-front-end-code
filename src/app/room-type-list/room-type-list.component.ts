import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HotelManagementService} from '../hotel-management.service';
import {RoomType} from '../model/room-type';
import {ConfirmationService, MessageService} from 'primeng/api';

@Component({
  selector: 'app-room-type-list',
  templateUrl: './room-type-list.component.html',
  styleUrls: ['./room-type-list.component.scss']
})
export class RoomTypeListComponent implements OnInit {
  allRoomTypes: RoomType[] = [];

  constructor(private router: Router, private service: HotelManagementService, private activatedRoute: ActivatedRoute, private confirmationService: ConfirmationService,  private messageService: MessageService) {
  }

  ngOnInit() {
    this.fetchAllRoomTypes();
    this.activatedRoute.params.subscribe(
      id => {
        const id1 = id.id;
        if (id1) {
          this.confirmDelete(id1);
        }
      }
    );
  }

  fetchAllRoomTypes() {
    this.service.getAllRoomTypes().subscribe(
      data => this.allRoomTypes = data
    );
  }

  gotoCreateRoomType() {
    this.router.navigateByUrl('create-room-type');
  }

  confirmDelete(id: string) {
    console.log('deleeeeeeeeeeee');
    this.confirmationService.confirm({
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
         this.service.deleteRoomType(id).subscribe(
           data => {
             return this.messageService.add({severity: 'success', summary: 'Success Message', detail: 'Room Type  Deleted Successfully..'});

           }
         );
         },
      reject: () => {
        return this.messageService.add({severity: 'error', summary: 'Error Message', detail: 'Room Type Deletion failed..'});
      }
    });
  }
}
