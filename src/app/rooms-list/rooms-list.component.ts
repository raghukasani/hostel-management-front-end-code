import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Route, Router} from '@angular/router';
import {HotelManagementService} from '../hotel-management.service';
import {Room} from '../model/room';
import {ConfirmationService, MessageService} from 'primeng/api';

@Component({
  selector: 'app-rooms-list',
  templateUrl: './rooms-list.component.html',
  styleUrls: ['./rooms-list.component.scss']
})
export class RoomsListComponent implements OnInit {

  allRooms: Room[] = [];
  constructor(private router: Router, private service: HotelManagementService, private confirmationService: ConfirmationService, private activatedRoute: ActivatedRoute, private messageService: MessageService) { }

  ngOnInit() {
    this.fetchAllRooms();
    this.activatedRoute.params.subscribe(
      id => {
        const id1 = id.id;
        if (id1) {
          this.confirmDelete(id1);
        }
      }
    );
  }

  gotoCreateRoom() {
    this.router.navigateByUrl('create-room');
  }
  fetchAllRooms() {
    this.service.getAllRooms().subscribe(
      data => this.allRooms = data
    );
  }

  private confirmDelete(id1: string) {
    console.log('deleeeeeeeeeeee');
    this.confirmationService.confirm({
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
         this.service.deleteRoom(id1).subscribe(
           data => {
             return this.messageService.add({severity: 'success', summary: 'Success Message', detail: 'Room  Deleted Successfully..'});
           }
         );
      },
      reject: () => {
        return this.messageService.add({severity: 'error', summary: 'Error Message', detail: 'Room  Deletion Failed..'});
      }
    });
  }
}
